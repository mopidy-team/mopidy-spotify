mopidy-spotify (4.1.1-0mopidy1) unstable; urgency=medium

  * New upstream release

 -- Stein Magnus Jodal <jodal@debian.org>  Thu, 24 Jun 2021 12:52:19 +0200

mopidy-spotify (4.1.0-0mopidy1) unstable; urgency=medium

  * New upstream release
  * d/watch: Use HTTPS
  * d/control: Set Rules-Requires-Root to no
  * d/control: Bump Standards-Version to 4.5.0
  * d/rules: Remove changelog override

 -- Stein Magnus Jodal <jodal@debian.org>  Sat, 26 Dec 2020 21:45:28 +0100

mopidy-spotify (4.0.1-0mopidy1) unstable; urgency=medium

  * New upstream release
  * d/copyright: Update copyright years

 -- Stein Magnus Jodal <jodal@debian.org>  Sat, 11 Jan 2020 10:35:44 +0100

mopidy-spotify (4.0.0-0mopidy1) unstable; urgency=medium

  * New upstream release
  * Switch from Python 2 to 3
  * Bump debhelper from 9 to 12
  * Update Maintainer's email address
  * d/clean: Remove custom clean rules
  * d/control: Bump Standards-Version to 4.4.1, no changes
  * d/control: Require mopidy >= 3
  * d/control: Update Description
  * d/control: Update Homepage, Vcs-Git and Vcs-Browser
  * d/copyright: Update copyright years
  * d/copyright: Use HTTPS for the copyright spec
  * d/rules: Update changelog name

 -- Stein Magnus Jodal <jodal@debian.org>  Sun, 29 Dec 2019 12:59:30 +0100

mopidy-spotify (3.1.0-0mopidy1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.8.
  * Update copyright years.

 -- Stein Magnus Jodal <jodal@debian.org>  Thu, 08 Jun 2017 00:12:49 +0200

mopidy-spotify (3.0.0-0mopidy1) unstable; urgency=medium

  * New upstream release
  * debian/rules
    - Install upstream changelog

 -- Stein Magnus Jodal <jodal@debian.org>  Tue, 16 Feb 2016 00:06:44 +0100

mopidy-spotify (2.3.1-0mopidy1) unstable; urgency=medium

  * New upstream release.

 -- Stein Magnus Jodal <jodal@debian.org>  Sun, 14 Feb 2016 22:05:27 +0100

mopidy-spotify (2.3.0-0mopidy1) unstable; urgency=medium

  * New upstream release.

 -- Stein Magnus Jodal <jodal@debian.org>  Sat, 06 Feb 2016 01:30:03 +0100

mopidy-spotify (2.2.0-0mopidy1) unstable; urgency=medium

  * New upstream release.
  * Clean dist/* and *.egg-info.
  * Fix repeated License block in debian/copyright.

 -- Stein Magnus Jodal <jodal@debian.org>  Sun, 15 Nov 2015 22:38:50 +0100

mopidy-spotify (2.1.0-0mopidy1) unstable; urgency=medium

  * New upstream release.
  * Build-Depend on python-requests.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Tue, 22 Sep 2015 21:33:08 +0200

mopidy-spotify (2.0.1-0mopidy1) unstable; urgency=medium

  * New upstream release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Sun, 23 Aug 2015 20:42:06 +0200

mopidy-spotify (2.0.0-0mopidy1) unstable; urgency=medium

  * New upstream release.
  * Remove /usr/share/mopidy/conf.d/spotify.conf. Cache and other data are
    now stored in paths defined by the core Mopidy package.
  * Depend on python-spotify >= 2, not < 2.
  * Build-Depend on mopidy, so it doesn't have to be included in
    pydist-override.
  * Build-Depend on debhelper 9.
  * Switch to pybuild as build system.
  * Build-Depend on python-spotify, so it doesn't have to be included in
    pydist-override.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Tue, 18 Aug 2015 00:13:51 +0200

mopidy-spotify (1.4.0-0mopidy1) unstable; urgency=medium

  * New upstream release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Tue, 19 May 2015 21:40:34 +0200

mopidy-spotify (1.3.0-0mopidy2) unstable; urgency=medium

  * Bump Standards-Version to 3.9.6.
  * Update copyright years.
  * Update watch file.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Fri, 27 Mar 2015 13:19:44 +0100

mopidy-spotify (1.3.0-0mopidy1) unstable; urgency=medium

  * New upstream release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Fri, 27 Mar 2015 13:05:22 +0100

mopidy-spotify (1.2.0-0mopidy3) unstable; urgency=medium

  * Add version specifier to python-spotify dependency. This package does not
    work with python-spotify >= 2.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Sun, 08 Mar 2015 01:17:21 +0100

mopidy-spotify (1.2.0-0mopidy2) unstable; urgency=medium

  * Move default config from /etc/mopidy/extensions.d/spotify.conf to
    /usr/share/mopidy/conf.d/spotify.conf.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Mon, 13 Oct 2014 22:58:15 +0200

mopidy-spotify (1.2.0-0mopidy1) unstable; urgency=medium

  * New upstream release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Mon, 21 Jul 2014 10:51:19 +0200

mopidy-spotify (1.1.3-0mopidy1) unstable; urgency=low

  * New upstream release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Tue, 18 Feb 2014 23:17:05 +0100

mopidy-spotify (1.1.2-0mopidy1) unstable; urgency=low

  * New upstream release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Tue, 18 Feb 2014 22:57:32 +0100

mopidy-spotify (1.1.0-0mopidy2) unstable; urgency=low

  * Fix permissions of /var/cache/mopidy/spotify and /var/lib/mopidy/spotify.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Fri, 24 Jan 2014 00:32:36 +0100

mopidy-spotify (1.1.0-0mopidy1) unstable; urgency=low

  * New upstream release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Wed, 22 Jan 2014 21:38:08 +0100

mopidy-spotify (1.0.3-0mopidy2) unstable; urgency=low

  * Add config file to /etc/mopidy/extensions.d for using /var/lib and
    /var/cache instead of XDG dirs when Mopidy is started from an init script.
  * Bump Standards-Version to 3.9.5.
  * Update copyright year.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Sun, 12 Jan 2014 02:55:05 +0100

mopidy-spotify (1.0.3-0mopidy1) unstable; urgency=low

  * New upstream release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Sun, 15 Dec 2013 21:06:53 +0100

mopidy-spotify (1.0.2-0mopidy1) unstable; urgency=low

  * New upstream release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Tue, 19 Nov 2013 23:53:09 +0100

mopidy-spotify (1.0.1-0mopidy3) unstable; urgency=low

  * Switch from mopidy-spotify.pydist to pydist-overrides for compatibility
    with wheezy.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Sun, 03 Nov 2013 22:13:23 +0100

mopidy-spotify (1.0.1-0mopidy2) unstable; urgency=low

  * Add python-setuptools to Build-Depends.
  * Convert from python-support to dh_python2.
  * Install as a private module.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Wed, 30 Oct 2013 23:24:39 +0100

mopidy-spotify (1.0.1-0mopidy1) unstable; urgency=low

  * Initial release

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Mon, 28 Oct 2013 21:26:43 +0100
